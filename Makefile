IMAGE := registry.gitlab.com/signal9/cpp-env
LLVM_VERSION := 14.0.6

.DEFAULT_GOAL := squash

.PHONY: docker
docker:
	@docker pull $(IMAGE):latest || true
	@docker build --pull -t $(IMAGE) --cache-from $(IMAGE):latest .

.PHONY: squash
squash: docker
	@docker-squash -t $(IMAGE):$(LLVM_VERSION) $(IMAGE):latest

.PHONY: push
push: squash
	@docker push $(IMAGE):$(LLVM_VERSION)
